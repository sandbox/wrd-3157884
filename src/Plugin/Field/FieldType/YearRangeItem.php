<?php

/**
 * @file
 * Contains \Drupal\year_range_field\Plugin\Field\Field\YearRangeItem.
 */

namespace Drupal\year_range_field\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;


/**
 * Plugin implementation of the 'address' field type.
 *
 * @FieldType(
 *   id = "year_range",
 *   label = @Translation("Year range"),
 *   description = @Translation("Stores a year range."),
 *   default_widget = "year_range_widget",
 *   default_formatter = "year_range_default"
 * )
 */
class YearRangeItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = [
      'columns' => [
        'start_year' => [
          'type' => 'int',
          'size' => 'small',
          'not null' => FALSE,
        ],
        'start_year_era' => [
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ],
        'end_year' => [
          'type' => 'int',
          'size' => 'small',
          'not null' => FALSE,
        ],
        'end_year_era' => [
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ],
        'to_present' => [
          'description' => 'Flag to control whether there is an end year or the range is open-ended.',
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
          'default' => 1,
        ],
      ],
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  static $propertyDefinitions;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['start_year'] = DataDefinition::create('integer')
      ->setLabel(t('Start year'));
    $properties['start_year_era'] = DataDefinition::create('string')
      ->setLabel(t('Start year era'));
    $properties['end_year'] = DataDefinition::create('integer')
      ->setLabel(t('End year'));
    $properties['end_year_era'] = DataDefinition::create('string')
      ->setLabel(t('End year era'));
    $properties['to_present'] = DataDefinition::create('boolean')
      ->setLabel(t('To present'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'require_end_year' => FALSE,
      'collect_eras' => FALSE,
      'require_eras' => FALSE,
      'allow_html_in_formatter' => FALSE,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('start_year')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['require_end_year'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require end year'),
      '#default_value' => $this->getSetting('require_end_year'),
      '#description' => $this->t('If enabled, there will be no "to present" option, and the end year will be required if a start year is entered.'),
    ];

    $element['collect_eras'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Collect eras'),
      '#default_value' => $this->getSetting('collect_eras'),
      '#description' => $this->t('If enabled, the user may select the era for each year (BCE or CE), which may be useful for historical date ranges.'),
    ];

    $element['require_eras'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require eras'),
      '#default_value' => $this->getSetting('require_eras'),
      '#description' => $this->t('Makes the selection of an era mandatory.'),
      '#states' => [
        'visible' => [
          ':input[name="settings[collect_eras]"]' => ['checked' => TRUE],
        ],
        'disabled' => [
          ':input[name="settings[collect_eras]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $element['allow_html_in_formatter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow HTML in formatter'),
      '#default_value' => $this->getSetting('allow_html_in_formatter'),
      '#description' => $this->t('Allow HTML to be used in the field formatter settings. Make sure that only trusted users have access to these settings.'),
    ];

    return $element;
  }

}
