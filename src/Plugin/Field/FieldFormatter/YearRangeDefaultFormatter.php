<?php

/**
 * @file
 * Contains \Drupal\year_range_field\Plugin\field\formatter\YearRangeDefaultFormatter.
 */

namespace Drupal\year_range_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;

/**
 * Plugin implementation of the 'year_range_default' formatter.
 *
 * @FieldFormatter(
 *   id = "year_range_default",
 *   label = @Translation("Year range default"),
 *   field_types = {
 *     "year_range"
 *   }
 * )
 */
class YearRangeDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $settings = $this->getSettings();
    $field_settings = $this->getFieldSettings();

    if ($field_settings['allow_html_in_formatter']) {
      $separator = new FormattableMarkup('@separator', ['@separator' => Xss::filterAdmin($settings['separator'])]);
      $to_present_string = new FormattableMarkup('@to_present_string', ['@to_present_string' => Xss::filterAdmin($settings['to_present_string'])]);
    } else {
      $separator = new FormattableMarkup('@separator', ['@separator' => Xss::filter($settings['separator'])]);
      $to_present_string = new FormattableMarkup('@to_present_string', ['@to_present_string' => Xss::filter($settings['to_present_string'])]);
    }

    $elements = [];
    foreach ($items as $delta => $item) {
      // Start year
      if (!$item->to_present and !empty($item->start_year_era) and $item->start_year_era != $item->end_year_era) {
        $start_year = $this->t('@year @era', ['@year' => $item->start_year, '@era' => $item->start_year_era]);
      } else {
        $start_year = $item->start_year;
      }

      // End year
      // TODO: make the "to present" text configurable
      if ($item->to_present) {
        $end_year = [
          '#type' => 'markup',
          '#markup' => Html::decodeEntities($to_present_string),
        ];
      } else {
        $end_year = (!empty($item->end_year_era) and $item->end_year_era != '_none') ? $this->t('@year @era', ['@year' => $item->end_year, '@era' => $item->end_year_era]) : $item->end_year;
        if ($end_year == $start_year) {
          $end_year = NULL;
        }
      }

      // Render output using year_range_default theme.
      $content = [
        '#theme' => 'year_range_default',
        '#start' => $start_year,
        '#separator' => [
          '#type' => 'markup',
          '#markup' => Html::decodeEntities($separator),
        ],
        '#end' => $end_year,
      ];

      $elements[$delta] = [
        '#markup' => \Drupal::service('renderer')->render($content)
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'separator' => ' &ndash; ',
      'to_present_string' => 'present',
    ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form = parent::settingsForm($form, $form_state);

    $form['separator'] = [
      '#title' => $this->t('Separator'),
      '#description' => $this->t('The string that should separate the years. For example, " to " will result in "2018 to 2020". Remember to include spaces if desired.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('separator'),
    ];

    $form['to_present_string'] = [
      '#title' => $this->t('"to present" string'),
      '#description' => $this->t('The string that should appear at the end of the range if the "to present" option is selected.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('to_present_string'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();
    $field_settings = $this->getFieldSettings();

    if ($field_settings['allow_html_in_formatter']) {
      $separator = Html::decodeEntities(Xss::filterAdmin($settings['separator']));
      $to_present_string = Html::decodeEntities(Xss::filterAdmin($settings['to_present_string']));
    } else {
      $separator = Html::decodeEntities(Xss::filter($settings['separator']));
      $to_present_string = Html::decodeEntities(Xss::filter($settings['to_present_string']));
    }

    $summary[] = $this->t('Separator: @separator', ['@separator' => $separator]);
    $summary[] = $this->t('"To present" text: @to_present_string', ['@to_present_string' => $to_present_string]);

    return $summary;
  }

}
