<?php

/**
 * @file
 * Contains \Drupal\year_range_field\Plugin\Field\FieldWidget\YearRangeWidget.
 */

namespace Drupal\year_range_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'year_range_widget' widget.
 *
 * @FieldWidget(
 *   id = "year_range_widget",
 *   module = "year_range_field",
 *   label = @Translation("Year range"),
 *   field_types = {
 *     "year_range"
 *   }
 * )
 */
class YearRangeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $field_settings = $this->getFieldSettings();

    // Used for select lists in optional Era inputs
    // TODO: Make this list configurable
    $era_options = [
      'BCE' => $this->t('BCE'),
      'CE' => $this->t('CE'),
    ];

    // Set up some selectors for #states
    if (!$this->isDefaultValueWidget($form_state)) {
      $start_year_selector = $this->t(':input[name="field_years[@delta][start_year_wrapper][start_year]"]', ['@delta' => $delta])->__toString();
      $end_year_selector = $this->t(':input[name="field_years[@delta][end_year_wrapper][end_year]"]', ['@delta' => $delta])->__toString();
      $to_present_selector = $this->t(':input[name="field_years[@delta][end_year_wrapper][to_present]"]', ['@delta' => $delta])->__toString();
    } else {
      $start_year_selector = $this->t(':input[name="default_value_input[field_years][@delta][start_year_wrapper][start_year]"]', ['@delta' => $delta])->__toString();
      $end_year_selector = $this->t(':input[name="default_value_input[field_years][@delta][end_year_wrapper][end_year]"]', ['@delta' => $delta])->__toString();
      $to_present_selector = $this->t(':input[name="default_value_input[field_years][@delta][end_year_wrapper][to_present]"]', ['@delta' => $delta])->__toString();
    }
    // Wrapper for the start year elements
    $element['start_year_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'start-year-wrapper',
        ],
      ],
    ];

    // The "Start year" element always displays
    $element['start_year_wrapper']['start_year'] = [
      '#title' => $this->t('Start year'),
      '#type' => 'number',
      '#step' => 1,
      '#min' => -32768,
      '#max' => 32767,
      '#default_value' => isset($items[$delta]->start_year) ? $items[$delta]->start_year : NULL,
    ];

    // The "Start year era" element displays if "collect_eras" is enabled
    if ($field_settings['collect_eras']) {
      $element['start_year_wrapper']['start_year_era'] = [
        '#title' => $this->t('Start year era'),
        '#type' => 'select',
        '#options' => $era_options,
        '#empty_option' => $this->t('- None -'),
        '#default_value' => isset($items[$delta]->start_year_era) ? $items[$delta]->start_year_era : NULL,
        '#required' => FALSE,
        '#states' => [],
      ];
    }

    // Wrapper for the end year elements
    $element['end_year_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'end-year-wrapper',
        ],
      ],
    ];

    // Both end_year elements only display if the "To present" box is unchecked
    $element['end_year_wrapper']['end_year'] = [
      '#title' => $this->t('End year'),
      '#type' => 'number',
      '#step' => 1,
      '#min' => -32768,
      '#max' => 32767,
      '#default_value' => isset($items[$delta]->end_year) ? $items[$delta]->end_year : NULL,
      '#states' => [
        'invisible' => [
          $to_present_selector => ['checked' => TRUE],
        ],
      ],
    ];

    // The "End year era" element also requires "collect_eras" to be enabled
    if ($field_settings['collect_eras']) {
      $element['end_year_wrapper']['end_year_era'] = [
        '#title' => $this->t('End year era'),
        '#type' => 'select',
        '#options' => $era_options,
        '#empty_option' => $this->t('- None -'),
        '#default_value' => isset($items[$delta]->end_year_era) ? $items[$delta]->end_year_era : NULL,
        '#states' => [
          'invisible' => [
            $to_present_selector => ['checked' => TRUE],
          ],
        ],
      ];
    }

    // If require_end_year is enabled, add states to the end year element
    if ($field_settings['require_end_year']) {
      $element['end_year_wrapper']['end_year']['#states']['required'] = [
        $start_year_selector => ['filled' => TRUE],
      ];
    } else {
      // Add the "to_present" element, but hide it if the "end_year" element contains
      // data.
      $element['end_year_wrapper']['to_present'] = [
        '#title' => $this->t('To present'),
        '#type' => 'checkbox',
        '#default_value' => $items[$delta]->to_present,
        '#description' => $this->t('If checked, no end year will be collected.'),
        '#states' => [
          'invisible' => [
            $end_year_selector => ['filled' => TRUE],
          ],
        ],
      ];
    }

    // Add various states to the era elements.
    $element['start_year_wrapper']['start_year_era']['#states']['disabled'] = [
      $start_year_selector => ['filled' => FALSE],
    ];
    $element['end_year_wrapper']['end_year_era']['#states']['disabled'] = [
      $end_year_selector => ['filled' => FALSE],
    ];
    if (!$this->isDefaultValueWidget($form_state) and $field_settings['require_eras']) {
      $element['start_year_wrapper']['start_year_era']['#states']['required'] = [
        $start_year_selector => ['filled' => TRUE],
      ];
      $element['end_year_wrapper']['end_year_era']['#states']['required'] = [
        $end_year_selector => ['filled' => TRUE],
      ];
    } else {
      $element['start_year_wrapper']['start_year_era']['#states']['invisible'] = [
        ':input[name="settings[collect_eras]"]' => ['checked' => FALSE],
      ];
      $element['end_year_wrapper']['end_year_era']['#states']['invisible'] = [
        ':input[name="settings[collect_eras]"]' => ['checked' => FALSE],
      ];
    }

    $element['#attached']['library'][] = 'year_range_field/default_widget';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      $values[$delta]['start_year'] = !empty($value['start_year_wrapper']['start_year']) ? (int) $value['start_year_wrapper']['start_year'] : NULL;
      $values[$delta]['start_year_era'] = !empty($value['start_year_wrapper']['start_year']) ? $value['start_year_wrapper']['start_year_era'] : NULL;
      $values[$delta]['end_year'] = !empty($value['end_year_wrapper']['end_year']) ? (int) $value['end_year_wrapper']['end_year'] : NULL;
      $values[$delta]['end_year_era'] = !empty($value['end_year_wrapper']['end_year']) ? $value['end_year_wrapper']['end_year_era'] : NULL;
      $values[$delta]['to_present'] = !empty($value['end_year_wrapper']['to_present']) ? $value['end_year_wrapper']['to_present'] : FALSE;
    }
    return $values;
  }

}
